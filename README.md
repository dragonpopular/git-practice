# git-practice

#### 介绍
git冲突管理练习

#### 安装说明（个人）

1.  安装[git bash](https://gitforwindows.org/)
2.  注册[gitee](https://gitee.com/)
3.  打开命令行界面，输入以下命令，修改git全局配置
    ```
    git config --global user.email "YOUR_GITEE_EMAIL"
    git config --global user.name "YOUR_NAME"
    ```
4. 配置[ssh key](https://help.gitee.com/base/account/SSH%E5%85%AC%E9%92%A5%E8%AE%BE%E7%BD%AE)

#### 练习说明（小组）

1. 每个组的组长folk当前仓库， 点击右上角folk按钮（组长）![avatar](resource/fork.png)
2. 组长将组员添加到folk后的仓库中（组长）![avatar](resource/add_member.png)
3. 将组长folk后的仓库克隆到自己的本地（个人）![avatar](resource/clone.png)
   ```
   git clone [copied repository link]
   ```
4. 修改team/team_member文件，在该文件中加上自己的名字（个人）
   ```
   git add .
   git commit -m "use your own message here"
   ```
5. 拉取远程仓库最新代码（个人）
    ```
    git pull -r
    ```
6. 如果没有冲突， 直接推送代码到远程分支。 注意：组里第一个提交代码的人不会遇到冲突， 如果想要进行冲突练习， 可以推送代码后等他人提交完再修改一下自己的名字，重复步骤4-6。 (个人)
   ```
    git push
    ```
   如果有冲突，先修改冲突文件,再推送代码到远程分支
    ```
    git add .
    git commit -m "resolve conflict when merge"
    git pull -r
    git push
    ```



